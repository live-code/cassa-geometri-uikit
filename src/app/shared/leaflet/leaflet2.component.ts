import { CommonModule } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import {
  AfterViewInit, ChangeDetectionStrategy,
  Component,
  ElementRef, inject, Input, NgZone, OnChanges, SimpleChanges,
  ViewChild
} from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-leaflet2',
  standalone: true,
  // changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule],
  template: `
    <div class="map" #host></div>
  `,
  styles: [`
    .map { height: 180px; }
  `]
})
export class Leaflet2Component implements OnChanges  {
  @Input() coords: any = [55, 13];
  @Input() zoom: number = 5;

  @ViewChild('host', { static: true }) host!: ElementRef<HTMLDivElement>
  map!: L.Map;

  constructor(private ngZone: NgZone) {
    const http = inject(HttpClient)
  }

  ngOnChanges(changes: SimpleChanges) {

    const { coords, zoom } = changes;
    if(!this.map) {
      this.init();
    }

    if(zoom) {
      this.map.setZoom(this.zoom)
    }
    if(coords) {
      this.map.setView(this.coords)
    }
  }

  init() {
    this.ngZone.runOutsideAngular(() => {
      this.map = L.map(this.host?.nativeElement)
        .setView(this.coords as any, this.zoom);
      L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(this.map);
    });
  }

  render() {
    console.log('rende')
  }
}
