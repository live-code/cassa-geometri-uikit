import { CommonModule } from '@angular/common';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef, Input,
  ViewChild
} from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'app-leaflet',
  standalone: true,
  // changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [CommonModule],
  template: `
    <div class="map" #host></div>
  `,
  styles: [`
    .map { height: 180px; }
  `]
})
export class LeafletComponent implements AfterViewInit  {
  @Input() title: string = ''
  @Input() set zoom(val: number) {
    this.zoomValue = val;
    if (this.map) {
      this.map.setZoom(val)
    }
  }
  zoomValue = 10;

  @Input() set coords(val: any) {
    this.coordsValue = val;
    if (this.map) {
      this.map.setView(val)
    }
  }
  coordsValue = [43, 13];

  @ViewChild('host') host!: ElementRef<HTMLDivElement>
  map!: L.Map;

  ngAfterViewInit() {
    this.map = L.map(this.host.nativeElement)
      .setView(this.coordsValue as any, this.zoomValue);
    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(this.map);
  }

}
