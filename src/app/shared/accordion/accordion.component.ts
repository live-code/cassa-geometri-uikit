import {
  AfterContentInit,
  AfterViewInit,
  Component,
  ContentChildren,
  Input,
  QueryList,
  ViewChildren
} from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from '../components/panel.component';

@Component({
  selector: 'app-accordion',
  standalone: true,
  imports: [CommonModule],
  template: `
   <div class="border border-dark p-2">
     <ng-content></ng-content>
   </div>
  `,
})
export class AccordionComponent implements AfterContentInit {
  @ContentChildren(PanelComponent) panels!: QueryList<PanelComponent>
  @Input() activeKey: string | undefined;
  @Input() closePrevious: boolean = true;

  ngAfterContentInit() {
    this.panels.toArray()[0].opened = true;

    this.panels.toArray().forEach((p, index) => {
      p.opened = p.key === this.activeKey;

      // p.key = 'tag' + index;
      p.toggle
        .subscribe(() => {
          if (this.closePrevious) {
            this.closeAll();
            p.opened = true;
          } else {
            p.opened = !p.opened
          }
        })
    })
  }

  closeAll() {
    this.panels.toArray()
      .forEach((p) => p.opened = false)
  }
}
