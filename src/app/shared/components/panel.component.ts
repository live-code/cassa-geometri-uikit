import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-panel',
  standalone: true,
  imports: [CommonModule],
  animations: [
    trigger('collapsable', [
      state('opened', style({
        height: '*',
        padding: '10px'
      })),
      state('closed', style({
        height: 0,
        padding: 0
      })),
      transition('opened <=> closed', [
        animate('1s cubic-bezier(0.83, 0, 0.17, 1)')
      ]),
    ])
  ],
  template: `
    <div class="card">
      <div class="card-header" 
           (click)="toggle.emit(key)">
        {{title}}
      </div>
      <div class="card-body mybody"
           [@collapsable]="opened ? 'opened' : 'closed'">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [`
    .mybody {
      overflow: hidden;
      padding: 0;
    }
  `]
})
export class PanelComponent {
  @Input() opened: boolean = false;
  @Input() title: string | undefined  ;
  @Input() key: string | undefined;
  @Output() toggle = new EventEmitter<string>();
}
