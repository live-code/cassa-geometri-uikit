import { Component, HostBinding, Inject, Injector, Input, Optional, Type } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FxComponent } from './fx.component';

@Component({
  selector: 'app-fx-col',
  standalone: true,
  imports: [CommonModule],
  template: `
    <ng-content></ng-content>
  `,
})
export class FxColComponent {
  @HostBinding() get class() {
    return 'flex-grow-' + (this.compo?.grow || 0);
    // return 'flex-grow-' + (this.fx?.grow || 0);
  }
  compo!: FxComponent | null;

  constructor(
    // @Optional() private fx: FxComponent,
    private inj: Injector
  ) {
    this.compo = this.inj.get(FxComponent, null, { optional: true})
  }
}

