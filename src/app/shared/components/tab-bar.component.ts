import { Component, EventEmitter, Input, Output, SimpleChanges } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-tab-bar',
  standalone: true,
  imports: [CommonModule],
  template: `
    <ul class="nav nav-tabs">
      <li 
        class="nav-item"
        *ngFor="let item of items"
        (click)="tabClick.emit(item)"
      >
        <a 
          class="nav-link"
          [ngClass]="{active: item.id === selectedItem?.id}"
        >{{item[labelField]}}</a>
      </li>
    </ul>
    
  `,
})
export class TabBarComponent<T extends {id: number, [key: string]: any}> {
  @Input() items: T[] = [];
  @Input() selectedItem: T | null = null;
  @Input() labelField = 'label';
  @Output() tabClick = new EventEmitter<T>()

  constructor() {
  }
}


