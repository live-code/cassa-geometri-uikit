import { Component, Input } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-separator',
  standalone: true,
  imports: [CommonModule],
  template: `
    <div 
      class="separator"
      [style.border-top-color]="color"
      [style.margin.px]="margin * 10"
      [style.border-top-style]="type"
    >
    </div>
  `,
  styles: [`
    .separator {
      border-top: 3px solid red;
      height: 1px;
    }
  `]
})
export class SeparatorComponent {
  @Input() color = 'black';
  @Input() margin: 0 | 1 | 2 | 3 = 1;
  @Input() type: 'solid' | 'dotted' | 'dashed' = 'solid'
}
