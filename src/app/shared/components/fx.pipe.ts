import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fx',
  standalone: true
})
export class FxPipe implements PipeTransform {

  transform(
    value: string,
    gap: number | string = 0,
  ): string[] {
    console.log('pipe')
    return [
      JUSTIFY[value],
      'gap-' + gap
    ]
  }

}



const JUSTIFY: any = {
  'start': 'justify-content-start',
  'between': 'justify-content-between',
  'end': 'justify-content-end',
  'around': 'justify-content-around'
}
