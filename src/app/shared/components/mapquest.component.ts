import { Component, Input, OnChanges } from '@angular/core';
import { CommonModule } from '@angular/common';

const baseURL = 'https://www.mapquestapi.com/staticmap/v5/map'
const token = 'Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn'

@Component({
  selector: 'app-mapquest',
  standalone: true,
  imports: [CommonModule],
  template: `
     <img 
      width="100" 
      [src]="url" alt=""
    >
  `,
})
export class MapquestComponent implements OnChanges {
  @Input() value!: string; // 43,13  | 'trieste'
  @Input() zoom = 10
  @Input() w = 300;
  @Input() h = 300;
  url: string = ''

  ngOnChanges(): void {
    this.url = `${baseURL}?key=${token}&center=${this.value}&zoom=${this.zoom}&size=${this.w},${this.h}`
  }


}
