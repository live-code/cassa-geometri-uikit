import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, ContentChildren, Input } from '@angular/core';
import { FxPipe } from './fx.pipe';

@Component({
  selector: 'app-fx',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [CommonModule, FxPipe],
  template: `
    <div 
      class="d-flex"
      [ngClass]="justify | fx: gap"
    >
      <ng-content />
    </div>
  `,
})
export class FxComponent {
  @Input() justify: 'around' | 'between' | 'end' | 'start' = 'start';
  @Input() gap: Gap = 0
  @Input() grow: 0 | 1 = 0
}

export type Gap = 0 | 1 | 2 | 3
