import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PadDirective } from './directives/pad.directive';
import { BgDirective } from './directives/bg.directive';
import { BorderDirective } from './directives/border.directive';
import { ThemeDirective } from './directives/theme.directive';
import { UrlDirective } from './directives/url.directive';
import { StopPropagationDirective } from './directives/stop-propagation.directive';
import { MyRouterLinkActiveDirective } from './directives/my-router-link-active.directive';
import { IfLoggedDirective } from './directives/if-logged.directive';
import { WidgetComponent } from './widget/widget.component';



@NgModule({
  declarations: [
    PadDirective,
    BgDirective,
    BorderDirective,
    ThemeDirective,
    UrlDirective,
    StopPropagationDirective,
    MyRouterLinkActiveDirective,
    IfLoggedDirective,

  ],
  imports: [
    CommonModule,
    WidgetComponent
  ],
  exports: [
    PadDirective,
    BgDirective,
    BorderDirective,
    ThemeDirective,
    UrlDirective,
    StopPropagationDirective,
    MyRouterLinkActiveDirective,
    IfLoggedDirective,
    WidgetComponent
  ]
})
export class SharedModule { }
