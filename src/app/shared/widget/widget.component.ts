import { Component } from '@angular/core';
import { FakeLogService } from '../../core/services/fakelog.service';
import { LogService } from '../../core/services/log.service';

@Component({
  selector: 'app-widget',
  standalone: true,
  template: `
    <p>
      widget works! {{log.count}}
    </p>
    
    <button (click)="log.add()"></button>
  `,
  providers: [
    { provide: LogService, useClass: FakeLogService }
  ]
})
export class WidgetComponent {
  constructor(public log: LogService) {
    log.doLog()
  }
}
