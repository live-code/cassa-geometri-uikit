import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { Inject, inject, InjectionToken, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '../environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FakeLogService } from './core/services/fakelog.service';
import { LogService } from './core/services/log.service';
import { SharedModule } from './shared/shared.module';

export const TOKEN = new InjectionToken('token')

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: LogService, useFactory: (http: HttpClient, token: number) => {
        return environment.production ? new FakeLogService(http, token) : new LogService(http, token);
      },
      deps: [HttpClient, TOKEN]
    },
    {
      provide: TOKEN,
      useFactory: () => 456
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }


