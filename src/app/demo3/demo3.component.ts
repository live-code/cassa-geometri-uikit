import { Component, Inject, TemplateRef, ViewChild, ViewContainerRef } from '@angular/core';
import { LogService } from '../core/services/log.service';

@Component({
  selector: 'app-demo3',
  template: `
    <p>
      demo3 - IfLogged directive
    </p>
    
    <button *appIfLogged="'admin'">AZIONI FANTASTICHE</button>
  `,
  styles: [
  ]
})
export class Demo3Component {
  constructor(private logService: LogService, @Inject('TOKEN') token: string) {
    console.log(token)
  }
}
