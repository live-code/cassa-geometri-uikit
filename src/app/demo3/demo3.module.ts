import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { Demo3RoutingModule } from './demo3-routing.module';
import { Demo3Component } from './demo3.component';


@NgModule({
  declarations: [
    Demo3Component
  ],
  imports: [
    CommonModule,
    Demo3RoutingModule,
    SharedModule
  ]
})
export class Demo3Module { }
