import { Component, ElementRef, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccordionComponent } from '../shared/accordion/accordion.component';
import { MapquestComponent } from '../shared/components/mapquest.component';
import { PanelComponent } from '../shared/components/panel.component';

@Component({
  selector: 'app-demo7',
  standalone: true,
  imports: [CommonModule, PanelComponent, AccordionComponent, MapquestComponent],
  template: `
  
    <app-accordion activeKey="one" [closePrevious]="true">
      <app-panel key="one" title="one">weiofewoifew</app-panel>
      <app-panel key="one" title="two">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aut corporis deserunt expedita magnam minus molestias nostrum porro vel, veritatis? Aliquid architecto ea est in nulla omnis perferendis quibusdam voluptate!
      </app-panel>
      <app-panel key="two" title="three">weiofewoifew</app-panel>
    </app-accordion>
    
    <!--<div [userId]="id"></div>
    <div tooltip="blalal"></div>-->
    
    <!--<div *ngFor="let item of items | filterByGender: 'M' | sortBy: 'ASC'"></div>-->
  `,
  styles: [
  ]
})
export default class Demo7Component {

}
