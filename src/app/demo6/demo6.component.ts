import { AfterViewInit, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeafletComponent } from '../shared/leaflet/leaflet.component';
import * as L from 'leaflet';
import { Leaflet2Component } from '../shared/leaflet/leaflet2.component';

@Component({
  selector: 'app-demo6',
  standalone: true,
  imports: [CommonModule, LeafletComponent, Leaflet2Component],
  template: `
    <app-leaflet2
      [zoom]="zoom"
      [coords]="coords"
    ></app-leaflet2>

    <app-leaflet
      [title]="'Leaflet '+ zoom"
      [zoom]="zoom"
      [coords]="coords"
    ></app-leaflet>
    
    <button (click)="zoom = zoom + 1">{{zoom}}</button>
    <button (click)="coords = [44, 14]">location 1</button>
    <button (click)="coords = [45, 14]">location 2</button>
  `,

})
export default  class Demo6Component {
  zoom = 5;
  coords = [45, 13]
}
