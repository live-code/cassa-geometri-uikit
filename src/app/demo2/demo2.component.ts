import { Component } from '@angular/core';
import { LogService } from '../core/services/log.service';

@Component({
  selector: 'app-demo2',
  template: `
    <button url="http://www.google.com">visit it</button>
    
    <div (click)="parent()" style="background: red; padding: 10px">
      parent
      <div (click)="child()" appStopPropagation style="background: cyan; padding: 10px">
        child
      </div>
      
    </div>
  `,
})
export class Demo2Component {

  constructor(log: LogService) {
    log.doLog()
  }
  parent() {
    console.log('parent')
  }

  child() {
    console.log('child')
  }
}
