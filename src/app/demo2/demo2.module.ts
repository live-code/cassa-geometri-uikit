import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { Demo2RoutingModule } from './demo2-routing.module';
import { Demo2Component } from './demo2.component';


@NgModule({
  declarations: [
    Demo2Component
  ],
  imports: [
    CommonModule,
    Demo2RoutingModule,
    SharedModule
  ]
})
export class Demo2Module { }
