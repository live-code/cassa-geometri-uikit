import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthService } from './core/auth.service';

const routes: Routes = [
  {
    path: 'demo1',
    canActivate: [() => inject(AuthService).isLogged$],
    loadChildren: () => import('./demo1/demo1.module').then(m => m.Demo1Module)
  },
  { path: 'demo2', loadChildren: () => import('./demo2/demo2.module').then(m => m.Demo2Module) },
  { path: 'demo3', loadChildren: () => import('./demo3/demo3.module').then(m => m.Demo3Module) },
  { path: 'demo4', loadComponent: () => import('./demo4/demo4.component')},
  { path: 'demo5', loadComponent: () => import('./demo5/demo5.component')},
  { path: 'demo6', loadComponent: () => import('./demo6/demo6.component')},
  { path: 'demo7', loadComponent: () => import('./demo7/demo7.component')},
  { path: '**', redirectTo: 'demo1'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
