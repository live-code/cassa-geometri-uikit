import { Component } from '@angular/core';
import { AuthService } from './core/auth.service';
import { ThemeService } from './core/theme.service';

@Component({
  selector: 'app-root',
  template: `
    
    <hr>
    <button (click)="themeSrv.change('dark')">dark</button>
    <button (click)="themeSrv.change('light')">light</button>
    <button (click)="authSrv.login()">login</button>
    <button (click)="authSrv.logout()">logout</button>
    <hr>
    <div>
      <button routerLink="demo1" appMyRouterLinkActive="bg-warning">demo1</button>
      <button routerLink="demo2" routerLinkActive="bg-warning">demo2</button>
      <button routerLink="demo3" routerLinkActive="bg-warning">demo3</button>
      <button routerLink="demo4" routerLinkActive="bg-warning">demo4</button>
      <button routerLink="demo5" routerLinkActive="bg-warning">demo5</button>
      <button routerLink="demo6" routerLinkActive="bg-warning">demo6</button>
      <button routerLink="demo7" routerLinkActive="bg-warning">demo7</button>
    </div>
    <hr>
    
    <div class="m-4">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: []
})
export class AppComponent {
  constructor(public themeSrv: ThemeService, public authSrv: AuthService) {

  }
}
