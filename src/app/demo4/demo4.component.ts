import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FxColComponent } from '../shared/components/fx-col.component';
import { FxComponent, Gap } from '../shared/components/fx.component';
import { MapquestComponent } from '../shared/components/mapquest.component';
import { SeparatorComponent } from '../shared/components/separator.component';

@Component({
  selector: 'app-demo4',
  standalone: true,
  imports: [
    CommonModule,
    SeparatorComponent,
    MapquestComponent,
    FxComponent,
    FxColComponent
  ],
  template: `
    <h2 >Layout</h2>

    <app-fx-col class="box">contenuto</app-fx-col>
    
    <app-fx justify="between" [grow]="1">
      <app-fx-col class="box">contenuto</app-fx-col>
      <app-fx-col class="box">contenuto</app-fx-col>
      <app-fx-col class="box">contenuto</app-fx-col>
    </app-fx>

    
    <app-fx justify="around" [gap]="gapValue">
      <div class="box flex-grow-1">4</div>
      <div class="box flex-grow-1">5</div>
      <div class="box flex-grow-1">6</div>
    </app-fx>

    <h2>Separator</h2>
    <app-separator color="red" />
    <app-separator [margin]="1" />
    <app-separator [margin]="3" />
    <app-separator type="dotted" />

    <h2>Map Quest</h2>
   <app-mapquest value="trieste" size="200,200"></app-mapquest>
   <app-mapquest [value]="city" [zoom]="zoom"></app-mapquest>
   <app-mapquest value="rome"></app-mapquest>
    <hr>
    <button (click)="zoom = zoom - 1">-</button>
    <button (click)="zoom = zoom + 1">+</button>
    <button (click)="city = 'roma'">roma</button>
    <button (click)="city = 'trieste'">trieste</button>
  `,
  styles: [`
    .box { background-color: red}
  `]
})
export default class Demo4Component {
    gapValue: Gap = 1
  zoom = 5;
  city = 'Palermo'

  constructor() {
    setTimeout(() => {
      this.gapValue =3
    }, 2000)
  }
}
