import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MapquestComponent } from '../shared/components/mapquest.component';
import { TabBarComponent } from '../shared/components/tab-bar.component';

@Component({
  selector: 'app-demo5',
  standalone: true,
  imports: [CommonModule, TabBarComponent, MapquestComponent, Demo5Component],
  template: `
    <p>
      demo5 works!
    </p>
    
    <app-tab-bar
      [items]="countries"
      [selectedItem]="selectedCountry"
      (tabClick)="selectCountry($event)"
    ></app-tab-bar>

    <app-tab-bar
      *ngIf="selectedCountry"
      labelField="name"
      [selectedItem]="selectedCity"
      [items]="selectedCountry.cities"
      (tabClick)="selectCity($event)"
    ></app-tab-bar>
    
    <app-mapquest
      *ngIf="selectedCity"
      [value]="selectedCity.name"></app-mapquest>
  `,
  styles: [
  ]
})
export default class Demo5Component {
  countries: Country[] = [];
  selectedCountry: Country | null = null
  selectedCity: City | null = null

  selectCountry(c: Country) {
    this.selectedCountry = c;
    this.selectedCity = this.selectedCountry.cities[0];
  }

  selectCity(c: City) {
    this.selectedCity = c;
  }

  constructor() {
    setTimeout(() => {
      this.countries = [
        {
          id: 2,
          label: 'germany',
          desc: 'bla bla 2',
          cities: [
            { id: 1, name: 'Berlin' },
            { id: 2, name: 'Monaco' }
          ]
        },
        {
          id: 1, label: 'italy', desc: 'bla bla 1',   cities: [
            { id: 11, name: 'Rome' },
            { id: 22, name: 'Milan' },
            { id: 33, name: 'Palermo' },
          ]
        },
        { id: 3, label: 'spain', desc: 'bla bla 3', cities: [
            {id: 41, name: 'Madrid'}
          ]},
      ];
      this.selectCountry(this.countries[0]);

    }, 1000)

  }

}



export interface City {
  id: number;
  name: string
}
export interface Country {
  id: number;
  label: string;
  desc: string;
  cities: City[]
}


