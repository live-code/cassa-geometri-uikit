import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { TOKEN } from '../../app.module';

@Injectable({
  providedIn: 'root'
})
export class FakeLogService {
  count = 0;
  
  constructor(private http: HttpClient,  @Inject(TOKEN) token?: number) {
    console.log('ctr fake log srv', token)
  }

  doLog() {
    this.count = this.count + 1;
    console.warn('fakelog !!!!', this.count)
  }

  add() {
    console.log('fake log')
    this.count++;
  }

}
