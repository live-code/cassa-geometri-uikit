import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, Optional } from '@angular/core';
import { TOKEN } from '../../app.module';

export interface ILog {
  doLog: () => void;
}

@Injectable({
  providedIn: 'root'
})
export class LogService implements ILog{
  count = 0;
  constructor(private http: HttpClient,  @Inject(TOKEN) token?: number) {
    console.log('ctr log srv', token)
  }

  doLog() {
    this.count = this.count + 1;
    console.log('log !!!!', this.count)
  }

  add() {
    console.log(' log')
    this.count++;
  }

}
