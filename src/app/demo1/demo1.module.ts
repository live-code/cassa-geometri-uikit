import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FakeLogService } from '../core/services/fakelog.service';
import { LogService } from '../core/services/log.service';
import { SharedModule } from '../shared/shared.module';
import { WidgetComponent } from '../shared/widget/widget.component';

import { Demo1RoutingModule } from './demo1-routing.module';
import { Demo1Component } from './demo1.component';


@NgModule({
  declarations: [
    Demo1Component,
  ],
  imports: [
    CommonModule,
    SharedModule,
    Demo1RoutingModule,
    FormsModule,
    WidgetComponent
  ],
/*  providers: [
    { provide: LogService, useClass: FakeLogService }
  ]*/

})
export class Demo1Module { }
