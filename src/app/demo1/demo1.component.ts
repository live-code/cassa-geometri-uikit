import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FakeLogService } from '../core/services/fakelog.service';
import { LogService } from '../core/services/log.service';

@Component({
  selector: 'app-demo1',
  template: `
    <button appThemed>THemed</button>
    
    <h3>appBorder: ngOnChanges</h3>
    <div 
      appBorder="3px"
      borderStyle="dashed"
      borderColor="cyan"
    >
      border</div>
    
    <h3>appBg: renderer2 e input set</h3>
    <div appBg="yellow">Background</div>
    <div [appBg]="{ color: 'white', bg: 'blue'}">Background</div>
    <div [appBg]="['bg-dark', 'text-white']">Background</div>
    
    <hr>
    <h3>appPad: hostBinding</h3>
    <input type="text"
           [(ngModel)]="value">

    <div [appPad]="0">ciao</div>
    
    <app-widget></app-widget>
    <app-widget></app-widget>
    
   `,

})
export class Demo1Component {
  value: string = 'purple';

  constructor(log: LogService) {
    log.doLog()
  }
}
